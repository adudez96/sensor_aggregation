#include <stdio.h>
#include <stdlib.h>

float createNoise (float noisyness) {
	float noise = ((float)rand()/(float)RAND_MAX);
	noise *= 2 * noisyness;
	noise -= noisyness;
	return noise;
}

void printFloatArray (float arr[], int length) {
	printf("Array at %p: [ ", arr);
	for (int i = 0; i < length; ++i) {
		printf("%f ", arr[i]);
		if (i != length - 1) printf(", ");
	}
	printf("]\n");
}

float** getReadings (float trueVal, int numSensors, float* biasVals, float noisyness, int numReadings) {
	// initialise array
	float** readings = malloc(sizeof(float*) * numReadings);
	// run through each reading
	for (int i = 0; i < numReadings; ++i) {
		// initialise array
		readings[i] = malloc(sizeof(float) * numSensors);
		// generate each sensor data
		for (int j = 0; j < numSensors; ++j) {
			float noise = createNoise(noisyness);
			readings[i][j] = trueVal + biasVals[j] + noise;
		}
		// printFloatArray(readings[i], numSensors);
	}

	return readings;
}

void freeReadings (float** readings, int numReadings) {
	for (int i = 0; i < numReadings; ++i) {
		free(readings[i]);
	}
	free(readings);
}

int main (int argc, char ** argv) {
	
	// IMPORTANT VARIABLES
	float trueVal = 0;
	int numSensors = 0;
	float biasVals[numSensors];
	float noisyness = 0;
	int numReadings = 0;

	// select true value
	printf("True value:   ");
	scanf("%f", &trueVal);

	// select number of sensors
	printf("Number of sensors:   ");
	scanf("%d", &numSensors);

	// select bias for each sensor
	for (int i = 0; i < numSensors; ++i) {
		printf("Bias for sensor %02d:   ", i);
		scanf("%f", &biasVals[i]);
	}

	// select noisyness level
	printf("Noisyness level:   ");
	scanf("%f", &noisyness);

	// select number of readings per sensor
	printf("Number of readings:   ");
	scanf("%d", &numReadings);

	// relay info to user
	printf("True value         = %f\n", trueVal);
	printf("Bias values        = ");
	printFloatArray(biasVals, numSensors);
	printf("Noisyness level    = %f\n", noisyness);
	printf("Number of readings = %d\n", numReadings);

	// generate readings
	float** readings = getReadings(trueVal, numSensors, biasVals, noisyness, numReadings);
	freeReadings(readings, numReadings);

	return EXIT_SUCCESS;
}